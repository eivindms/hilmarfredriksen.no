// Libraries
import React from 'react';
import Router from 'react-router';

// Components
import Links from './navlinks';

// Main navbar layout, the links are in separate component
class NavBar extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-inverse navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>

          <div className="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <Links />
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;

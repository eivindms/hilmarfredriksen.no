// Libraries
import React from 'react';
import {DefaultRoute, Route, Routes, RouteHandler} from 'react-router';
import createActiveRouteComponent from 'react-router-active-component'; // Automatic 'active' class for li elements

var Link = createActiveRouteComponent('li');

// Links for the main nav bar
class NavLinks extends React.Component {
  render() {
    return (
      <ul className="nav navbar-nav">
        <Link to="home">Home</Link>
        <Link to="about">About</Link>
        <Link to="gallery">Gallery/Media</Link>
      </ul>
    );
  }
}

export default NavLinks;

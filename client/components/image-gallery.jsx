// Libraries
import React from 'react';
import Router from 'react-router';

// Simple element layout, used primarily for images/videos
class GalleryComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var images = [];

    this.props.children.forEach(function(element, index, array) {
      images.push(
        <div key={"gallery-wrapper-" + index} style={{display: 'block', marginBottom: 15}} className="col-lg-3 col-md-4 col-sm-6 col-xs-12"> {element} </div>
      );

      // Fairly hacky way of using bootstrap to layout the gallery
      // adding grid class depending on index in list (with clearfix, to keep row height)
      if((index + 1) % 2 == 0) {

        if((index + 1) % 4 == 0) {
          images.push(<div key={"gallery-wrapper-clearfix-" + index} className="clearfix visible-lg-block visible-sm-block" />);
        } else {
          images.push(<div key={"gallery-wrapper-clearfix-" + index} className="clearfix visible-sm-block" />);
        }
      } else if((index + 1) % 3 == 0) {
        images.push(<div key={"gallery-wrapper-clearfix-" + index} className="clearfix visible-md-block" />);
      }
    });

    return (
      <div className="container-fluid" style={{paddingTop: 90 }}>
        <div className="row">
          {images}
        </div>
      </div>
    );
  }
}

export default GalleryComponent;

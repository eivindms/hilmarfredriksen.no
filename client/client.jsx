// Polyfill
require('babel/polyfill')

// Libraries
import React from 'react';
import Router from 'react-router';

// Routes
import routes from './routes';


// Router config
var AppRouter = Router.create({
  routes: routes,
  scrollBehavior: Router.ImitateBrowserBehavior,
  location: Router.HistoryLocation
});


// Where it all starts (client side)
AppRouter.run(
  (Handler) => React.render(<Handler />, document.getElementById('content'))
);

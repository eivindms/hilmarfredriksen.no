// Libraries
import React from 'react';
import ImageLoader from 'react-imageloader';

// Needs some actual content
class About extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div style={{marginTop: 90}} className='container'>
        <ImageLoader src={require('./about/hilmar.jpg')} style={{float: 'right' }} />
        <h1>Hilmar Fredriksen</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. At iste non dolendi status non vocatur voluptas. Minime vero, inquit ille, consentit. Haec para/doca illi, nos admirabilia dicamus. Duo Reges: constructio interrete. Equidem e Cn. <a href='http://loripsum.net/' target='_blank'>An nisi populari fama?</a> </p>

        <ul>
          <li>In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret.</li>
          <li>Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem illam Aristippi in prima commendatione poneret.</li>
          <li>Primum cur ista res digna odio est, nisi quod est turpis?</li>
          <li>Sin ea non neglegemus neque tamen ad finem summi boni referemus, non multum ab Erilli levitate aberrabimus.</li>
        </ul>


        <dl>
          <dt><dfn>Venit ad extremum;</dfn></dt>
          <dd>Sed quid ages tandem, si utilitas ab amicitia, ut fit saepe, defecerit?</dd>
          <dt><dfn>Quibusnam praeteritis?</dfn></dt>
          <dd>Istam voluptatem, inquit, Epicurus ignorat?</dd>
        </dl>


        <h3>Aperiendum est igitur, quid sit voluptas;</h3>

        <p>Quare conare, quaeso. <i>Quodsi ipsam honestatem undique pertectam atque absolutam.</i> At modo dixeras nihil in istis rebus esse, quod interesset. Ex rebus enim timiditas, non ex vocabulis nascitur. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Primum cur ista res digna odio est, nisi quod est turpis? <a href='http://loripsum.net/' target='_blank'>Quonam, inquit, modo?</a> </p>

        <blockquote cite='http://loripsum.net'>
          Sed quid attinet de rebus tam apertis plura requirere?
        </blockquote>


        <ol>
          <li>Quamquam id quidem licebit iis existimare, qui legerint.</li>
          <li>Atque ab isto capite fluere necesse est omnem rationem bonorum et malorum.</li>
          <li>Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?</li>
          <li>Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.</li>
        </ol>


        <p>Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Sed vos squalidius, illorum vides quam niteat oratio. <mark>Certe, nisi voluptatem tanti aestimaretis.</mark> At ille pellit, qui permulcet sensum voluptate. <i>Non laboro, inquit, de nomine.</i> Facillimum id quidem est, inquam. Cur deinde Metrodori liberos commendas? Maximus dolor, inquit, brevis est. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. <i>Eam stabilem appellas.</i> Hanc quoque iucunditatem, si vis, transfer in animum; </p>

        <p>De ingenio eius in his disputationibus, non de moribus quaeritur. <mark>Haec dicuntur inconstantissime.</mark> Qui autem esse poteris, nisi te amor ipse ceperit? At iam decimum annum in spelunca iacet. Cupiditates non Epicuri divisione finiebat, sed sua satietate. Duarum enim vitarum nobis erunt instituta capienda. Age nunc isti doceant, vel tu potius quis enim ista melius? Sed finge non solum callidum eum, qui aliquid improbe faciat, verum etiam praepotentem, ut M. </p>

        <h2>Consequentia exquirere, quoad sit id, quod volumus, effectum.</h2>

        <p>Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur. <i>Istam voluptatem, inquit, Epicurus ignorat?</i> Sed ea mala virtuti magnitudine obruebantur. Quae similitudo in genere etiam humano apparet. At certe gravius. Animum autem reliquis rebus ita perfecit, ut corpus; Equidem e Cn. <a href='http://loripsum.net/' target='_blank'>Summae mihi videtur inscitiae.</a> </p>

      </div>
    );
  }
}

export default About;

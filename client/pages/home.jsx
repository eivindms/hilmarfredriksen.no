// Libraries
import React from 'react';
require('../../bootstrap.config.less');

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
      <div style={{
          background: 'url("' + require("./home/hilmar-vogn.jpg") + '") top center no-repeat, black',
          height: 500,
          paddingTop: 120
          }} id="ole">
      <div className='container'>
        <div style={{paddingRight: 15, paddingLeft: 15, background: 'black', position: 'absolute'}}>
          <h1 style={{color: '#ffffff'}}>Hilmar Fredriksen</h1>
        </div>
      </div>
      </div>
      </div>
    );
  }
}

//export default Radium.Enhancer(App);
export default Home;

// Libraries
import React from 'react';
import { RouteHandler } from 'react-router';

// Parent page for gallery functions
class Gallery extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    // Render CRUD views
    return (
      <div>
        <RouteHandler />
      </div>
    );
  }
}

export default Gallery;

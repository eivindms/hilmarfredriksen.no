// Components
import GalleryActions from './gallery-actions';
import alt from '../../alt'; // Flux

class GalleryStore {
  constructor() {
    // Full articles with full-size images/video - loaded as needed
    this.items = [];

    // Thumbnails and short descriptions - load all
    this.itemList = [];

    this.bindListeners({
      handleLoadItemList: GalleryActions.GET_ITEMS_FROM_SERVER,
      handleLoadDisplayItem: GalleryActions.GET_FULL_ITEM_FROM_SERVER
    });

    this.handleLoadItemList = this.handleLoadItemList.bind(this);
    this.handleLoadDisplayItem = this.handleLoadDisplayItem.bind(this);
  }

  handleLoadDisplayItem(item) {
    // Check if item is already loaded
    var idx = this.items.length > 0 ? this.items.findIndex(function(element) {
      return element._id === item._id
    }) : -1;

    if(idx == -1) {
      // If new, push to array
      this.items.push(item);
    } else {
      // If exists, update values
      this.items[idx] = item;
    }
  }

  handleLoadItemList(items) {
    // Replace whole list with updated values
    this.itemList = items;
  }
}

export default alt.createStore(GalleryStore, 'GalleryStore');

// Libraries
import React from 'react';

// Components
import GalleryStore from './gallery-store';
import GalleryActions from './gallery-actions';
import Form from './form-component';

class GalleryEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = GalleryStore.getState();
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    GalleryStore.listen(this.onChange);
    GalleryActions.getItemsFromServer();
  }

  componentDidUnmount() {
    GalleryStore.unlisten(this.onChange);
  }

  onChange(state) {
    this.setState(state);
  }

  handleSubmit(data) {
    GalleryActions.updateItem(data);
  }

  render() {
    let self = this;
    var filtered = this.state.items.filter(function(item) {return item._id === self.props.params.id});

    // Expects requested element to be the only one in the list
    if(filtered.length != 1) {
      return (
      <div style={{marginTop: 90}} className='container'>
        <h2>Item not found</h2>
      </div>
      )
    } else {
      let el = filtered[0];
      return (
        <div style={{marginTop: 90}} className='container'>
          <div className="row">
            <Form model={el} onSubmit={this.handleSubmit} />
          </div>
        </div>
      );
    }
  }
}

export default GalleryEdit;

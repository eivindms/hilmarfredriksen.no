// Libraries
import React from 'react';

// Components
import GalleryActions from './gallery-actions';
import Form from './form-component';

class GalleryAdd extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange(state) {
    this.setState(state);
  }

  handleSubmit(data) {
    console.log("Submit received!");
    console.log(data);
    GalleryActions.addItem(data);
  }

  render() {
    let el = {
      title: null,
      subTitle: null,
      description: null,
      category: null,
      created: null
    };
      return (
        <div style={{marginTop: 90}} className='container'>
          <div className="row">
            <Form model={el} onSubmit={this.handleSubmit} />
          </div>
        </div>
      );
  }
}

export default GalleryAdd;

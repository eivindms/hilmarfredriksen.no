// Libraries
import request from 'superagent';
import alt from '../../alt'; // Flux


// All operations from the gallery page (split up?)
class GalleryActions {
  getFullItemFromServer(id) {
    var self = this;
    request('GET', '/api/items/' + id).end(function(err, res) {
      if(res.ok) {
        //setTimeout(function(){
          self.dispatch(res.body);
        //}, 3000)
      } else {
        // Do something here
        console.log(err);
      }
    })
  }

  getItemsFromServer() {
    var self = this;
    request('GET', '/api/items').end(function(err, res) {
      if(res.ok) {
        self.dispatch(res.body);
      } else {
        // Do something here
        console.log(err);
      }
    })
  }

  addItem(data) {
    var self = this;
    request('POST', '/api/items').set('Content-Type', 'application/json').send(data).end(function(err, res) {
      if(res.ok) {
        // OK, now update Item list
        request('GET', '/api/items').end(function(err2, res2) {
          if(res2.ok) {
            self.dispatch(res2.body);
          } else {
            // Do something here
            console.log(err2);
          }
        })
      } else {
        // Do something here
        console.log(err);
      }
    })
  }

  updateItem(data) {
    var self = this;
    request('PUT', '/api/items/' + data._id).send(data).end(function(err, res) {
      if(res.ok) {
        // OK, now update Item list
        request('GET', '/api/items').end(function(err2, res2) {
          if(res2.ok) {
            self.dispatch(res2.body);
          } else {
            // Do something here
            console.log(err2);
          }
        });
        request('GET', '/api/items/' + data._id).end(function(err, res) {
          if(res.ok) {
            self.dispatch(res.body);
          } else {
            // Do something here
            console.log(err);
          }
        });
      } else {
        // Do something here
        console.log(err);
      }
    })
  }
}

export default alt.createActions(GalleryActions);

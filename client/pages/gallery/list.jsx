// Libraries
import React from 'react';
import { Link } from 'react-router';
import ImageLoader from 'react-imageloader';

// Components
import ComponentGallery from '../../components/image-gallery';
import GalleryStore from './gallery-store';
import GalleryActions from './gallery-actions';

class List extends React.Component {
  constructor(props) {
    super(props);

    this.state = GalleryStore.getState();
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    GalleryStore.listen(this.onChange);
    GalleryActions.getItemsFromServer();
  }

  componentDidUnmount() {
    GalleryStore.unlisten(this.onChange);
  }

  onChange(state) {
    this.setState(state);
  }

  render() {
    var images = this.state.itemList.map(function(el, index) {
        // Markup for selected element (in view/:id param)
        return (
          <div>
            <Link to="view-item" params={{id: el._id}}>View    </Link>
            <Link to="edit-item" params={{id: el._id}}>Edit</Link>
            <h3>{el.title}</h3>
            <h3>{el.subTitle}</h3>
            <ImageLoader key={"bilde" + index} style={{width: 100 + '%'}} src={'data:' + el.media.mimeType + ';base64,' + el.media.data}>
              <h3>Not loaded</h3>
            </ImageLoader>
          </div>
        )
    });

    return (
      <div>
        <ComponentGallery>{images}</ComponentGallery>
      </div>
    );
  }
}

export default List;

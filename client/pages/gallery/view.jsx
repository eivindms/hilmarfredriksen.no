// Libraries
import React from 'react';
import { Link } from 'react-router';
import ImageLoader from 'react-imageloader';
import Radium from 'radium';

// Components
import ComponentGallery from '../../components/image-gallery';
import GalleryStore from './gallery-store';
import GalleryActions from './gallery-actions';

let style = {
  paddingTop: 90
};

class View extends React.Component {
  constructor(props) {
    super(props);

    this.state = GalleryStore.getState();
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    GalleryStore.listen(this.onChange);
    GalleryActions.getFullItemFromServer(this.props.params.id);
  }

  componentDidUnmount() {
    GalleryStore.unlisten(this.onChange);
  }

  onChange(state) {
    this.setState(state);
  }

  render() {
    var item = this.state.items.length > 0 ? this.state.items.find(function(el) {
      return el._id === this.props.params.id;
    }, this) : undefined;

    if(typeof item === 'undefined') {
    return (
      <div>
        <h1>Not found!</h1>
      </div>
    );

    } else {
    return (
      <div>
          <div style={style} className="container-fluid">
            <div style={{}} className="container">
              <div className="row">
                <div className="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                  <Link to="gallery">Close</Link>
                  <h2>{item.title}</h2>
                  <h3>{item.subTitle}</h3>
                  <p>{item.description}</p>
                </div>
                <div className="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                  <ImageLoader style={{width: 100 + '%'}} src={'data:' + item.media.mimeType + ';base64,' + item.media.data}>
                    <h3>Not loaded</h3>
                  </ImageLoader>
                </div>
              </div>
            </div>
          </div>
      </div>
    );

    }
  }
}

export default Radium(View);

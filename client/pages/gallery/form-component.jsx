// Libraries
import React from 'react';

// Components
import Dropzone from 'react-dropzone';

// The input form used in Add/Edit actions
class FormComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.props.model;

    // Change handlers for inputs
    this.onTitleChange = this.onTitleChange.bind(this);
    this.onSubTitleChange = this.onSubTitleChange.bind(this);
    this.onDescriptionChange = this.onDescriptionChange.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onCreatedChange = this.onCreatedChange.bind(this);
    this.handleDrop = this.handleDrop.bind(this);

    // Add validation here?
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleDrop(files) {
    var self = this;
    var filereader = new FileReader();
    // console.log('Received files: ', files[0]);
    filereader.readAsBinaryString(files[0]);

    filereader.onloadend = function() {
      self.setState({
        media: {
          mimeType: 'image/png',
          dataType: 'base64',
          data: btoa(filereader.result)
        }
      })
    }
  }

  onTitleChange(element) {
    this.setState({title: element.target.value});
  }
  onSubTitleChange(element) {
    this.setState({subTitle: element.target.value});
  }
  onDescriptionChange(element) {
    this.setState({description: element.target.value});
  }
  onCategoryChange(element) {
    this.setState({category: element.target.value});
  }
  onCreatedChange(element) {
    this.setState({created: element.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    event.stopPropagation();

    this.props.onSubmit(this.state);
  }

  render() {
    var image =  typeof(this.state.media) != 'undefined' ? (
      <img style={{width: '100%'}} src={"data:image/png;base64," + this.state.media.data} />
    ) : null;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="row">
          <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <input type="text" className="form-control" onChange={this.onTitleChange} placeholder="Title" value={this.state.title} />
            <input type="text" className="form-control" onChange={this.onSubTitleChange} placeholder="Sub Title" value={this.state.subTitle} />
            <input type="text" className="form-control" onChange={this.onDescriptionChange} placeholder="Description" value={this.state.description} />
            <input type="text" className="form-control" onChange={this.onCategoryChange} placeholder="Category" value={this.state.category} />
            <input type="text" className="form-control" onChange={this.onCreatedChange} placeholder="Created" value={this.state.created} />
            <Dropzone onDrop={this.handleDrop} size={255} >
              <div>Try dropping some files here, or click to select files to upload.</div>
            </Dropzone>
          </div>
          <div className="col-lg-9 col-md-8 col-sm-6 col-xs-12">
            {image}
          </div>
        </div>
        <div className="row">
          <button className="btn btn-primary" type="submit">Save</button>
        </div>
      </form>
    );
  }
}

export default FormComponent;

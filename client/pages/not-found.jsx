// Libraries
import React from 'react';

// Handler for various "Not found" routes
class NotFound extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <h1>You've come to the wrong place!</h1>
    );
  }
}

export default NotFound;

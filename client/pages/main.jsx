// Libraries
import React from 'react';
import { RouteHandler } from 'react-router';

// Components
import NavBar from '../components/navbar';

// Main markup for all other pages
class Main extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <NavBar site="Hilmar Fredriksen" />
        <div className=''>
          <RouteHandler />
        </div>
        <div className='container'>
          <footer>©Hilmar Fredriksen 2015</footer>
        </div>
      </div>
    );
  }
}

export default Main;

// Libraries
import React from 'react';
import {RouteHandler} from 'react-router';

// Main route for rendering the app routes. Bare-bones, but some
// markup may be added later
class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <RouteHandler />
    );
  }
}

export default App;

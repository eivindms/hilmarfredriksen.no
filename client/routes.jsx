import React from 'react';
import { NotFoundRoute, Route, DefaultRoute } from 'react-router';


// Import all handlers (needs async)
import Home from './pages/home';
import Main from './pages/main';
import NotFound from './pages/not-found';
import App from './app';
import Gallery from './pages/gallery'
import GalleryInput from './pages/gallery/edit';
import GalleryAdd from './pages/gallery/add';
import GalleryView from './pages/gallery/view';
import GalleryList from './pages/gallery/list';
import About from './pages/about'

// Routes for the entire application
export default (
  <Route name="app" handler={App}>
    <Route name="main" path="/" handler={Main}>
    <DefaultRoute name="home" handler={Home} />

    <Route name="about" path="about" handler={About} />
    <Route name="gallery" path="gallery" handler={Gallery}>
      <Route name="edit-item" path="edit/:id" handler={GalleryInput} />
      <DefaultRoute name="all-items" handler={GalleryList} />
      <Route name="view-item" path="view/:id" handler={GalleryView} />
      <Route name="add-item" path="new" handler={GalleryAdd} />
    </Route>
    </Route>
    <NotFoundRoute handler={NotFound} />
  </Route>
)

HilmarFredriksen.no
===================

This is the website I am working on for Hilmar, as a project on the side. I
want to use new JavaScript technology like React.JS to build it, so that I can
familiarize myself with it. Currently the site sports an about page, a "home"
page for news, and a small, quirky media gallery.

Libraries
---------

1. React with server side rendering
2. React-Router
3. WebPack
4. NodeJS


Road Map
-------- 
* Utilize MongoDB for ~~image storage~~ and news
* Implement authentication logic for Admin only (Updating, etc.)
* Add tests (not necessary at this point)
* Generally better UI (find components)
* Find hosting / deployment solution

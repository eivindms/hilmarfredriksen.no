// Libraries
var React = require("react");
var Router = require('react-router');

require('../bootstrap.config.less');

// Load routes
var routes = require('../client/routes.jsx');

module.exports = function(req, scriptFilename) {
	var html;
	var router = Router.create({location: req.url, routes: routes})
	router.run(function(Handler, state) {
		html = React.renderToString(<Handler />);
	})

	return React.renderToString(
		<html>
			<head>
				<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			</head>
			<body>
				<div id="content" dangerouslySetInnerHTML={{__html: html}} />
				<script src="https://code.jquery.com/jquery-git2.min.js"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				<script src={"/assets/" + scriptFilename}></script>
			</body>
		</html>
	);
}

// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Define schema
var itemSchema = new mongoose.Schema({
  title: String,
  subTitle: String,
  description: String,
  category: String,
  media: {
    mimeType: String,
    dataType: String, // Url, Base64, etc.
    data: String
  },
  thumbnail: String, // Base64 encoded image
  created: Date
});

// Return model (CommonJS module, not loaded by WebPack)
module.exports = restful.model('Items', itemSchema);

// Dependencies
var express = require('express');
var router = express.Router();

// Models
var GalleryItem = require('./models/items');

// Routes
GalleryItem.methods(['get', 'post', 'put', 'patch', 'delete']);
GalleryItem.register(router, '/items')

// Return router (CommonJS module, not loaded by WebPack)
module.exports = router;

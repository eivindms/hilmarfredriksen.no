var path = require("path");

var commonLoaders = [
  // Needed for the css-loader when [bootstrap-webpack](https://github.com/bline/bootstrap-webpack)
  // loads bootstrap's css.
  { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&minetype=application/font-woff" },
  { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&minetype=application/font-woff" },
  { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=application/octet-stream" },
  { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
  { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=image/svg+xml" },

	{ test: /\.js$/, loader: "babel-loader", include: [path.join(__dirname, 'client'), path.join(__dirname, 'server')] },
	{ test: /\.jsx$/, loader: "babel-loader", include: [path.join(__dirname, 'client'), path.join(__dirname, 'server')] },
	{ test: /\.(png|jpg)$/, loader: "url-loader" }
];
var commonResolve = { extensions: ['', '.js', '.jsx'] };
var assetsPath = path.join(__dirname, "public", "assets");
var publicPath = path.join(__dirname, "public", "assets");

module.exports = [
	{
		//devtool: 'source-map',
		// The configuration for the client
		name: "browser",
		entry: "./client/client",
		output: {
			path: assetsPath,
			filename: "[hash].js",
			publicPath: publicPath
		},
		module: {
			loaders: commonLoaders.concat([
				{ test: /\.css$/, loader: "style-loader!css-loader" },
				{ test: /\.less$/, loader: "style!css!less" }
			])
		},
		resolve: commonResolve,
		plugins: [
			function(compiler) {
				this.plugin("done", function(stats) {
					require("fs").writeFileSync(path.join(__dirname, "server", "stats.generated.json"), JSON.stringify(stats.toJson()));
				});
			}
		]
	},
	{
		// The configuration for the server-side rendering
		name: "server-side rendering",
		entry: "./server/page.js",
		target: "node",
		output: {
			path: assetsPath,
			filename: "../../server/page.generated.js",
			publicPath: publicPath	,
			libraryTarget: "commonjs2"
		},
		externals: /^[a-z\-0-9]+$/,
		resolve: commonResolve,
		module: {
			loaders: commonLoaders.concat([
				{ test: /\.css$/,  loader: path.join(__dirname, "server", "style-collector") + "!css-loader" },
				{ test: /\.less$/,  loader: path.join(__dirname, "server", "style-collector") + "!css!less" }
			])
		}
	}
];
